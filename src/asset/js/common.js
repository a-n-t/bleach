/**
 * siteオブジェクトを定義
 */
var site = site || {};


/**
 * 本ファイルまでの相対パス
 */
var PATH = (function () {
	var scripts = document.getElementsByTagName('script');
	var src = scripts[scripts.length - 1].getAttribute('src');
	var index = src.lastIndexOf('/');
	return (index !== -1) ? src.substring(0, index) : '';
})();


// グローバル変数
var $document = $(document);
var $window = $(window);
var $BREAKPOINT = 740;


/**
 * リンクの別ウィンドウ表示
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 */
site.setLinkNewWindow = function($) {
    var $targets = $('a[href^="http://"], a[href^="https://"], a[data-rel="external"]').not('a[href^="http://'+ location.hostname +'"], a[href^="https://'+ location.hostname +'"], a[data-rel="trigger"]');
    $targets.on('click', function() {
        open($(this).attr('href'), null);
        return false;
    });
}


/**
 * スムーズスクロール
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setSmoothScroll = function($){
    $(function(){
        $('a[href^="#"]').click(function() {
            var speed = 400;
            var href= $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top - 100 + 'px';
            $('body,html').animate({scrollTop:position}, speed, 'swing');
            return false;
        });
    });
}


/**
 * ナビゲーション
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
var navigation = {
    setMotion: function() {
        var $button = $('.l-nav__ul__item--btn');
        
        var scaleWindowW = function() {
            var w = (window.innerWidth || document.documentElement.clientWidth || 0);
            return w;
        };

        window.addEventListener('DOMContentLoaded', function() {
            var initAccordion = function() {
                if (scaleWindowW() < 768) {
                    $button.each(function(){
                        $(this).on('click', function() {
                            if($(this).hasClass('selected')) {
                                $(this).removeClass('selected');
                                $(this).find('.l-nestnav').slideUp();
                            } else {
                                $(this).addClass('selected');
                                $(this).find('.l-nestnav').slideDown();
                            }
                        });
                    });
                } else if (scaleWindowW() >= 768) {
                    $button.hover(function() {
                        $(this).children('ul').stop(true, true).slideDown(300);
            
                    }, function() {
                        $(this).children('ul').stop(true, true).delay(200).slideUp(300);
                    });
                }
            }
            initAccordion();

            window.addEventListener('resize',initAccordion);
        }, false);
    }
}
navigation.setMotion();


/**
 * トップへ戻る
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setBackTop = function($) {
    var $backtopButton = $('.l-footer__backtop');
    $backtopButton.hide();
    $(document).ready(function() {
        var pagetop = $backtopButton;
            $(window).scroll(function () {
                if ($(this).scrollTop() > 200) {
                    pagetop.fadeIn();
                } else {
                    pagetop.fadeOut();
                }
            });
            pagetop.click(function () {
            $('body, html').animate({ scrollTop: 0 }, 500);
                return false;
        });
    });
}


/**
 * ハンバーガーメニュー
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setHamburgerMenu = function($) {
    var $hamburgerButton = $('.l-line');
    var $gnav = $('.l-header__gnav');

    $hamburgerButton.on('click', function() {
        if($hamburgerButton.hasClass('js-active')) {
            $('body').css({
                "overflow": "visible",
                "height": "auto"
            });
            $gnav.stop().slideUp();
            $hamburgerButton.removeClass('js-active');
        } else {
            $('body,html').css({"overflow":"visible","height":"auto"});
            $gnav.stop().slideDown();
            $gnav.css('display', 'block');
            $hamburgerButton.addClass('js-active');
        }
    });

    $window.on('resize', function() {
        var w = $window.width();

        if($BREAKPOINT < w) {
            $gnav.show();
            $gnav.css('display', 'flex');
        } else {
            $gnav.css('display', 'block');
            $gnav.hide();
            
        }
    });
}


/**
 * 画面領域に入ったらフェードイントランジション
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setFadeInTransition = function($) {
    var $fromTop = $('.c-fromtop');
    var $fromBottom = $('.c-frombottom');
    var $fromRight = $('.c-fromright');
    var $fromLeft = $('.c-fromleft');

    $fromTop.on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        $(this).velocity({
            properties: { 
                translateY: 30,
                opacity: 1
            },
            options: { duration: 800, deray: 0, easing: 'easeOutQuart' }
        });
    });
    $fromBottom.on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        $(this).velocity({
            properties: { 
                translateY: -30,
                opacity: 1
            },
            options: { duration: 800, deray: 0, easing: 'easeOutQuart' }
        });
    });
    $fromRight.on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        $(this).velocity({
            properties: { 
                right: 0,
                opacity: 1
            },
            options: { duration: 500, deray: 0, easing: 'easeOutQuart' }
        });
    });
    $fromLeft.on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        $(this).velocity({
            properties: { 
                left: 0,
                opacity: 1
            },
            options: { duration: 500, deray: 0, easing: 'easeOutQuart' }
        });
    });



    $('.c-scale').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        $(this).addClass('js-animate');
    });
}

/**
 * SVG - Vivus
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.svgVivus = function($) {
    if(!$('.c-main__cover').length) return; 

    setTimeout(() => {
        $('#canvaspcbefore').css('opacity', 1);
        $('#canvasspbefore').css('opacity', 1);

        // PC SVG Animation
        new Vivus('canvaspcbefore', {start: 'autostart', type: 'delayed', duration: 70}, function(obj) {
            obj.el.classList.add('fill');
        }); 
        setTimeout(() => {
            $('#canvaspc').css('opacity', 1);

            new Vivus('canvaspc', {start: 'autostart', type: 'delayed', duration: 70,}, function(obj) {
                obj.el.classList.add('fill');
            }); 
        }, 30);
        
        // SP SVG Animation
        new Vivus('canvasspbefore', {start: 'autostart', type: 'delayed', duration: 70}, function(obj) {
            obj.el.classList.add('fill');
        }); 
        setTimeout(() => {
            $('#canvassp').css('opacity', 1);
            
            new Vivus('canvassp', {start: 'autostart', type: 'delayed', duration: 70,}, function(obj) {
                obj.el.classList.add('fill');
            }); 
        }, 30);
    }, 70);   
}

/*
 * 読み込み完了時の処理
 */
jQuery(function($) {
    site.setLinkNewWindow($);
    site.setSmoothScroll($);
    site.setBackTop($);
    site.setHamburgerMenu($);
    site.setFadeInTransition($);
    site.svgVivus($);
});


