var $window = $(window);
var $BREAKPOINT = 740;

/**
 * Interview - SLICK
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setSlick = function($) {
    var $itemContent = $('.t-slide');

    // スマホ時スライドに切り替える
    var slider = $itemContent.bxSlider({
        auto: true,
        mode: 'fade',
        infiniteLoop: true,
        speed: 1200,
        autoControls: true
    });

    function sliderSetting() {
        var w = $window.width();

        if(w <= $BREAKPOINT) {
            slider.reloadSlider();
        } else {
            slider.destroySlider();
        }
    }

    $window.on('load resize', function(){
        sliderSetting();
    });
}


/*
 * 読み込み完了時の処理
 */
jQuery(function($) {
    site.setSlick($);
});