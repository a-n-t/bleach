/**
 * トップ - ブラウザ領域に入ったらアニメーション
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setInviewAnimate = function($) {
    var $openWindow = $('.t-window__image');

    $openWindow.on('inview', function(event, isInView, visiblePartX, visiblePartY) {
        $(this).addClass('js-active');
    });
}


/**
 * トップ - SLICK
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setSlick = function($) {
    var $heroContent = $('.c-main__slide');
    var $itemContent = $('.t-slide');

    $heroContent.slick({
        autoplay: true,
        fade: true,
        autoplaySpeed: 4000,
        speed: 1000
    });

    $itemContent.bxSlider({
        auto: true,
        mode: 'fade',
        infiniteLoop: true,
        speed: 1200,
        autoControls: true
    });

    // $('.bx-pager').hide();
    // $('.bx-controls-auto').hide();
    // $('.bx-controls-direction').hide();
}


/*
 * 読み込み完了時の処理
 */
jQuery(function($) {
    site.setInviewAnimate($);
    site.setSlick($);
});