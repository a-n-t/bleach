<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

/* ========================================================================
    ローカル環境
 * ====================================================================== */
// // // ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
// // /** WordPress のためのデータベース名 */
// define('DB_NAME', 'bleach-blog');

// /** MySQL データベースのユーザー名 */
// define('DB_USER', 'root');

// /** MySQL データベースのパスワード */
// define('DB_PASSWORD', 'root');

// /** MySQL のホスト名 */
// define('DB_HOST', 'localhost');

// /** データベースのテーブルを作成する際のデータベースの文字セット */
// define('DB_CHARSET', 'utf8mb4');

// /** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
// define('DB_COLLATE', '');

/* ========================================================================
    テスト環境
 * ====================================================================== */
// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'ant_wp_bleach');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'ant');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'x2xa5t3qN2mY');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql635.db.sakura.ne.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');
/* ========================================================================*/

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|KWa-gNQRbti;QT)pHj$w*fkoHK~g&vL(YfJ8Zv]<?&rziYCI6{uY@ps?P`S,-Gu');
define('SECURE_AUTH_KEY',  'JAZB61y$vP8myup*9GLN2Gz{/Y53Ou=X^8n}J5MKcZsPHVV$dEspO0@bE$.C[kop');
define('LOGGED_IN_KEY',    'P-M&tR6&*dm^`a@9) )4)M/;JnKbM#gP5iF-ni,-ZJ~hjZ7XOJvXrlg`}=y.2UV5');
define('NONCE_KEY',        '8!LFn$1zkVF_;w8<9B<Q6IOI5q pt;T3|kLR`Yac}t |L8N];ripwyT{%015;3];');
define('AUTH_SALT',        '6eYG|.m[BO8s4>7JamTN)V2#F{IcPh2^Dxoi!&2{;z!W.4C8cg`fZP)7 rDbM.HI');
define('SECURE_AUTH_SALT', ',F{0:n>;%2d]9rc5Ik7u26p@7Um3M%|_eFX(J.qkvukS Lig,9zf@4R mhL%t~#~');
define('LOGGED_IN_SALT',   'QO?wiSogH+<BvU1n,^-^;JJl)@Mu`p@-8)u?6k5k}qa&P26nipKUA4dm~$;,t<cQ');
define('NONCE_SALT',       '8y2cW2CALl HYw_u542wv5CO{=6vZ&.q^)87jnJUm=5(tfFk5hfdMJq6]2.5[e(u');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);
// define('WP_HOME','http://localhost:8888');
// define('WP_SITEURL','http://localhost:8888');

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
