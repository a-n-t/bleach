/**
 * siteオブジェクトを定義
 */
var site = site || {};


/**
 * 本ファイルまでの相対パス
 */
var PATH = (function () {
	var scripts = document.getElementsByTagName('script');
	var src = scripts[scripts.length - 1].getAttribute('src');
	var index = src.lastIndexOf('/');
	return (index !== -1) ? src.substring(0, index) : '';
})();


// グローバル変数
var $document = $(document);
var $window = $(window);
var $BREAKPOINT = 740;


/**
 * リンクの別ウィンドウ表示
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 */
site.setLinkNewWindow = function($) {
    var $targets = $('a[href^="http://"], a[href^="https://"], a[data-rel="external"]').not('a[href^="http://'+ location.hostname +'"], a[href^="https://'+ location.hostname +'"], a[data-rel="trigger"]');
    $targets.on('click', function() {
        open($(this).attr('href'), null);
        return false;
    });
}


/**
 * スムーズスクロール
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setSmoothScroll = function($){
    $(function(){
        $('a[href^="#"]').click(function() {
            var speed = 400;
            var href= $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top - 100 + 'px';
            $('body,html').animate({scrollTop:position}, speed, 'swing');
            return false;
        });
    });
}


/**
 * ナビゲーション
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setNavigation = function($) {
    var $nestnav = $('.l-nestnav');
    var $nestnav_btn = $('.l-nav__ul__item--btn');
    var $mainnav = $('.l-nav__main__item');

    $window.on('load', function() {
        var w = $window.width();

        if($BREAKPOINT < w) {
            set_pc();
        } else {
            set_sp();
        }
    });

    function set_pc() {
        $nestnav_btn.hover(function() {
            $(this).children('ul').stop(true, true).slideDown(300);

        }, function() {
            $(this).children('ul').stop(true, true).delay(200).slideUp(300);
        });
    }

    function set_sp() {
        $nestnav_btn.on('click', function() {
            if($(this).hasClass('selected')) {
                $(this).removeClass('selected').children('ul').slideUp();
            } else {
                $nestnav_btn.removeClass('selected');
                $nestnav.hide();
                $(this).addClass('selected').children('ul').slideDown();
            }
        });
    }
}


/**
 * トップへ戻る
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setBackTop = function($) {
    var $backtopButton = $('.l-footer__backtop');
    $backtopButton.hide();
    $(document).ready(function() {
        var pagetop = $backtopButton;
            $(window).scroll(function () {
                if ($(this).scrollTop() > 200) {
                    pagetop.fadeIn();
                } else {
                    pagetop.fadeOut();
                }
            });
            pagetop.click(function () {
            $('body, html').animate({ scrollTop: 0 }, 500);
                return false;
        });
    });
}


/**
 * ハンバーガーメニュー
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setHamburgerMenu = function($) {
    var $hamburgerButton = $('.l-line');
    var $gnav = $('.l-header__gnav');

    $hamburgerButton.on('click', function() {
        if($hamburgerButton.hasClass('js-active')) {
            $('body').css({
                "overflow": "visible",
                "height": "auto"
            });
            $gnav.stop().slideUp();
            $hamburgerButton.removeClass('js-active');
        } else {
            $('body,html').css({"overflow":"visible","height":"auto"});
            $gnav.stop().slideDown();
            $gnav.css('display', 'block');
            $hamburgerButton.addClass('js-active');
        }
    });

    $window.on('resize', function() {
        var w = $window.width();

        if($BREAKPOINT < w) {
            $gnav.show();
            $gnav.css('display', 'flex');
        } else {
            $gnav.css('display', 'block');
            $gnav.hide();
            
        }
    });
}


/**
 * カテゴリータグアコーディオン
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setCategoryAccordion = function($) {
    if(!$('.t-tags__ul').length) return;

    var $cat_btn = $('.t-tags__btn').find('a');
    var $cat_img = $cat_btn.find('img');
    var $cat_item = $('.t-tags__ul__item');
    var $m;

    $cat_btn.on('click', function() {
        if($m.hasClass('js-active')) {
            $cat_img.removeClass('js-active');
            $m.removeClass('js-active');
            $m.slideUp();
        } else {
            $cat_img.addClass('js-active');
            $m.addClass('js-active');
            $m.slideDown();
        }
    });

    $document.ready(function(){
        if(window.matchMedia("(max-width:740px)").matches){
            // SP
            $m = $('.t-tags__ul__item');
            $m.hide();
            $cat_btn.show();
        }
        else {
            // PC
            $m = $('.t-tags__ul__item:gt(3)');
            $m.hide();

            setBtnShowOrHide();
        }
        
        function matchFunction (){
            // SP
            $m = $('.t-tags__ul__item');
            $m.hide();
            $cat_btn.show();

            if(window.matchMedia("(min-width:740px)").matches){
                // PC
                $m = $('.t-tags__ul__item:gt(3)');
                $('.t-tags__ul__item').show();
                $m.hide();

                setBtnShowOrHide();
            }
        }
        window.matchMedia("(max-width:740px)").addListener(matchFunction);
    });

    function setBtnShowOrHide () {
        var l = $cat_item.length;
        if(l < 5) {
            $cat_btn.hide();
        }
    }
}

/**
 * ページネーションテキスト
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setPagenationText = function($) {
    if(!$('.t-pagenation--cover').length) return;

    $document.ready(function(){
        if(window.matchMedia("(max-width:740px)").matches){
            // SP
            $('.prev').text('');
            $('.next').text('');
        }
        else{
            // PC
            $('.prev').text('PREV');
            $('.next').text('NEXT');
        }
        
        function matchFunction(){
            // SP
            $('.prev').text('');
            $('.next').text('');

            if(window.matchMedia("(min-width:740px)").matches){
                // PC
                $('.prev').text('PREV');
                $('.next').text('NEXT');
            }
        }
        window.matchMedia("(max-width:740px)").addListener(matchFunction);
    });
}


/**
 * 同じカテゴリー記事のテキスト表示/非表示
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.sameCategoryOption = function($) {
    if(!$('.t-item').length) return;

    var l = $('.t-item--cover--js').length;
    if(l == 0) {
        $('.t-item__heading').hide();
    }
}


/**
 * 全て読み込んだらフェードイン
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 * @require Colorbox 1.6.4
 */
site.setFooterImage = function($) {
    if(!$('.l-footer__image').length) return;

    var $box = $('.l-footer__image');
    var $footer_image = $box.find('img');

    $footer_image.imagesLoaded().always(function() {
        $box.velocity({
            p: { opacity: 1 },
            o: { duration: 500 }
        });
    });
}




/*
 * 読み込み完了時の処理
 */
jQuery(function($) {
    site.setLinkNewWindow($);
    site.setSmoothScroll($);
    site.setNavigation($);
    site.setBackTop($);
    site.setHamburgerMenu($);
    site.setCategoryAccordion($);
    site.setPagenationText($);
    site.sameCategoryOption($);
    site.setFooterImage($);
});


