<?php get_header(); ?>

<?php
    $category = get_the_category();
    $cat_id   = $category[0]->cat_ID;
    $cat_name = $category[0]->cat_name;
    $cat_slug = $category[0]->category_nicename;
?>


            <div class="l-frame" id="business">
                <!-- ===================================================================================================== -->
                <ul class="c-breadcrumb">
					<li class="c-breadcrumb__item"><a href="../">株式会社ブリーチ採用情報&nbsp;/&nbsp;</a></li>
					<li class="c-breadcrumb__item"><a href="../business/">Blog</a></li>
                </ul>
                <!-- ===================================================================================================== -->
                <h2 class="c-h2"><span>B</span>usiness Blog</h2>
                <!-- ===================================================================================================== -->
                <div class="l-main">
                    <div class="l-col">
                        <div class="t-detail">
                            <p class="t-detail__category">
                                <?php echo $cat_name; ?>
                            </p>
                            <p class="t-detail__date">
                                <?php echo get_the_date('Y.m.d'); ?>
                            </p>
                        </div>

                        <div class="t-single">
                            <p class="t-single__title"><?php the_title(); ?></p>

                            <div class="t-single__image">
                            <?php 
                                // アイキャッチ画像が設定されているかチェック
                                if(has_post_thumbnail()){
                                    // アイキャッチ画像を表示する
                                    the_post_thumbnail();
                                }else{
                                    // 代替画像を表示する
                                    echo '<img src="/asset/image/dummy01.jpg" width="640" height="384" alt="No Image" />';
                                }
                            ?>
                            </div>
                            
                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                                <p class="c-p"><?php the_content(); ?></p>
                            <?php endwhile; else : ?>
                                <p class="c-p"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!-- ===================================================================================================== -->
                <div class="t-item">
                    <p class="t-item__heading"><span>同じカテゴリーの記事</span></p>

                    <!-- 新着順に3件表示する -->
                    <?php
                        $post_id = get_the_ID();
                        foreach((get_the_category()) as $cat) {
                            $cat_id = $cat->cat_ID ;
                            break ;
                        }
                        query_posts(
                            array(
                                'cat' => $cat_id,
                                'showposts' => 3,
                                'post__not_in' => array($post_id)
                            )
                        );
                        if(have_posts()) :
                    ?>
                    <div class="t-item--cover l-col l-col--sb l-col--fw">
                        <?php while (have_posts()) : the_post(); ?>
                            <div class="l-col__3 t-item--cover--js">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="t-item__image">
                                        <p class="t-article__box__category"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></p>
                                        <?php the_post_thumbnail('large_thumbnail', array('alt' => the_title_attribute('echo=0'), 'title' => the_title_attribute('echo=0'))); ?>
                                    </div>
                                    <p class="t-item__title"><?php the_title(); ?></p>
                                    <p class="t-item__summary">
                                        <?php 
                                            if(mb_strlen($post->post_content, 'UTF-8') > 50){
                                                $content= mb_substr($post->post_content, 0, 74, 'UTF-8');
                                                echo $content.'…';
                                            }else{
                                                echo $post->post_content;
                                            }
                                        ?>
                                    </p>
                                    <p class="t-item__date"><?php echo get_the_date('Y.m.d'); ?></p>
                                </a>
                            </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
                <!-- ===================================================================================================== -->
            </div>


<?php get_footer(); ?>