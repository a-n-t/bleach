
            <div class="l-footer">
				<div class="l-footer__backtop"><a href=""><img src="<?php bloginfo('template_url'); ?>/asset/image/backtop.png" alt=""></a></div>
				<!-- <div class="l-footer__image"><img src="./wp-content/themes/bleach-blog/asset/image/footer-img01.jpg" alt=""></div> -->
				<div class="l-frame">
					<div class="l-frame--md">
						<div class="l-col">
							<!-- <div class="l-footer__link"><a class="c-ng" href="https://job.mynavi.jp/19/pc/corpinfo/displaySeminar/index?optNo=SHhL-C&amp;corpId=229819" target="_blank"><span>NEW GRADUATE</span><br>新卒採用エントリー</a></div>
							<div class="l-footer__link"><a class="c-mc" href="https://www.wantedly.com/companies/bleach/" target="_blank"><span>MID-CAREER RECRUITMENT</span><br>中途採用エントリー</a></div> -->
						</div>
					</div>
				</div>
				<div class="l-frame">
					<p class="l-footer__text">SHARE</p>
					<div class="l-col--sp l-footer__sns">
						<div class="l-footer__sns__item"><a href="http://www.facebook.com/share.php?u=http://bleach.co.jp/recruit.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/asset/image/icon-facebook.png" alt=""></a></div>
						<div class="l-footer__sns__item"><a href="http://twitter.com/share?&amp;url=http://bleach.co.jp/recruit.html&amp;hashtags=株式会社ブリーチ採用情報" target="_blank"><img src="<?php bloginfo('template_url'); ?>/asset/image/icon-twitter.png" alt=""></a></div>
					</div>
				</div>
				<div class="l-frame">
					<div class="l-footer__copyright"><small>Copyright c BLEACH.Inc.All.Rights.Reserved.</small></div>
				</div>
			</div>
        </div>
        <script src="<?php bloginfo('template_url');?>/asset/js/min/jquery-3.1.1.min.js"></script>
		<script src="<?php bloginfo('template_url');?>/asset/js/min/imagesloaded.pkgd.min.js"></script>
		<script src="<?php bloginfo('template_url');?>/asset/js/min/velocity.min.js"></script>
		<script src="<?php bloginfo('template_url');?>/asset/js/min/jquery.inview.min.js"></script>
        <script src="<?php bloginfo('template_url');?>/asset/js/common.js"></script>
        <?php wp_footer(); ?>
    </body>
</html>