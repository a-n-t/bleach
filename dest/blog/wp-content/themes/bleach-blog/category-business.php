<?php get_header(); ?>


            <div class="l-frame" id="business">
                <h2 class="c-h1"><a href="<?php echo get_rel_home(); ?>"><b>BLEACH</b><br><span>B</span>usiness Blog</a></h2>



                <div class="t-tags">
                    <p class="t-tags__select">カテゴリーを選択</p>
                    <?php 
                        $cat = get_category_by_slug("business");
                        $cat_id = $cat->cat_ID;

                        $args = array(
                            'orderby' => 'id',
                            'parent' => $cat_id
                        );
                        $categories = get_categories( $args );
                        usort($categories, function($a, $b) {
                            return get_field("表示順", "category_".$a->term_id) - get_field("表示順", "category_".$b->term_id);
                        });
                    ?>

                    <ul class="t-tags__ul l-col l-col--sb l-col--fw">
                        <?php 
                            // echo '<li class="t-tags__ul__item"><a href="/bleach/blog/' . $cat->slug . '/">' . $cat->name . '</a></li>'
                        ?>
                        <?php
                            foreach ( $categories as $category ) {
                                $cat_link = get_category_link($category->cat_ID);
                                echo '<li class="t-tags__ul__item"><a href="' . $cat_link . '">' . $category->name . '</a></li>';
                            }
                        ?>
                    </ul>
                    <div class="t-tags__btn"><a href="#"><img src="<?php bloginfo('template_url'); ?>/asset/image/business-open.png" alt=""></a></div>
                </div>



                <div class="l-main">
                    <div class="t-article--cover l-col l-col--sb l-col--fw">

                        <!-- ============================================================================================================================================================================ -->
                        <?php
                            if (have_posts()) : 
                                while (have_posts()) :
                                    the_post();
                                    get_template_part('inc/each_exrpt_post');
                                endwhile;
                            endif;
                        ?>
                        <!-- ============================================================================================================================================================================ -->

                    </div>
                    <!-- ============================================================================================================================================================================ -->
                    <!-- ページネーション -->
                    <?php get_template_part('inc/pagination'); ?>
                    <!-- ============================================================================================================================================================================ -->

                </div>
            </div>


<?php get_footer(); ?>