                    <!-- ============================================================================================================================================================================ --><!-- ============================================================================================================================================================================ -->
                    <!-- メンバーインタビュー -->
                    <div class="t-interview">
                        <div class="t-interview__title"><img src="<?php bloginfo('template_url'); ?>/asset/image/interview-title.png" alt=""></div>
                        <div class="l-col--sp l-col--sb l-col--fw l-col__4--cover">
                            <?php
                                $loop = new WP_Query(array("post_type" => "team"));
                                if ( $loop->have_posts() ) : while($loop->have_posts()): $loop->the_post();
                            ?>
                            <div class="l-col__4 t-interview--box">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail('full'); ?>
                                    <p class="t-interview__name"><?php the_title(); ?></p>
                                </a>
                            </div>
                            <?php endwhile; endif; ?>
                        </div>
                    </div>
                    <!-- ============================================================================================================================================================================ -->