                            <div class="t-article l-col__3">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="t-article__box">
                                        <p class="t-article__box__category"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></p>
                                        <div class="t-article__box__image"><?php the_post_thumbnail('large_thumbnail', array('alt' => the_title_attribute('echo=0'), 'title' => the_title_attribute('echo=0'))); ?></div>
                                    </div>
                                    <p class="t-article__title"><?php the_title(); ?></p>
                                    <p class="t-article__summary">
                                        <?php 
                                            if(mb_strlen($post->post_content, 'UTF-8') > 50){
                                                $content= mb_substr($post->post_content, 0, 40, 'UTF-8');
                                                echo $content.'';
                                            }else{
                                                echo $post->post_content;
                                            }
                                        ?>
                                    </p>
                                    <p class="t-article__date"><?php echo get_the_date('Y.m.d'); ?></p>
                                </a>
                            </div>