<?php
/**
 * Template Name: トップページ */
?>
<?php get_header(); ?>


            <div class="l-frame">
                <h2 class="c-title">Blog</h2>

                <p class="c-lede">BLEACHが手掛けるビジネスのノウハウや、<br>BLEACHの採用情報をスタッフがお届けします。</p>

                <div class="l-frame--md">
                    <div class="l-col l-col--sb l-col--fw">
                        <div class="l-col--2 t-top t-top--business">
                            <a href="http://bleach.co.jp/blog/business/">
                                <p class="t-top__name">BLEACH</p>
                                <p class="t-top__title"><span>B</span>usiness Blog</p>

                                <p class="t-top__text">BLEACHが手掛ける<br class="u-hidden-sp">ビジネスのノウハウをお届けします。</p>
                            </a>
                        </div>
                        <div class="l-col--2 t-top t-top--recruiting">
                            <a href="http://bleach.co.jp/blog/recruit/">
                                <p class="t-top__name">BLEACH</p>
                                <p class="t-top__title"><span>R</span>ecruiting Blog</p>

                                <p class="t-top__text">BLEACHの採用情報を<br class="u-hidden-sp">スタッフがお届けします。</p>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- <div class="l-frame--md">
                    <div class="l-col l-col--sb l-col--fw">
                        <div class="l-col--2 t-top t-top--business">
                            <a href="<?php bloginfo('url'); ?>/business/">
                                <p class="t-top__name">BLEACH</p>
                                <p class="t-top__title"><span>B</span>usiness Blog</p>

                                <p class="t-top__text">BLEACHが手掛ける<br class="u-hidden-sp">ビジネスのノウハウをお届けします。</p>
                            </a>
                        </div>
                        <div class="l-col--2 t-top t-top--recruiting">
                            <a href="<?php bloginfo('url'); ?>/recruit/">
                                <p class="t-top__name">BLEACH</p>
                                <p class="t-top__title"><span>R</span>ecruiting Blog</p>

                                <p class="t-top__text">BLEACHの採用情報を<br class="u-hidden-sp">スタッフがお届けします。</p>
                            </a>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="l-footer__image"><img src="<?php bloginfo('template_url'); ?>/asset/image/footer-img01.jpg" alt=""></div>


<?php get_footer(); ?>