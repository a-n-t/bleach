<?php get_header(); ?>


            <div class="l-frame" id="business">
                <h2 class="c-h1"><b>BLEACH</b><br><span>B</span>usiness Blog</h2>





                <div class="t-tags">
                    <?php 
                        $args = array(
                            'orderby' => 'id',
                            'child_of' => 0
                        );
                        $categories = get_categories( $args );
                        usort($categories, function($a, $b) {
                            return get_field("表示順", "category_".$a->term_id) - get_field("表示順", "category_".$b->term_id);
                        });
                    ?>
                    <ul class="t-tags__ul l-col l-col--fw">
                        <?php
                            foreach ( $categories as $category ) {
                                $cat_link = get_category_link($category->cat_ID);
                                echo '<li class="t-tags__ul__item"><a href="' . $cat_link . '">' . $category->name . '</a></li>';
                            }
                        ?>
                    </ul>
                    <div class="t-tags__btn"><a href="#"><img src="<?php bloginfo('template_url'); ?>/asset/image/business-open.png" alt=""></a></div>
                </div>





                <div class="l-main">
                    <div class="t-article--cover l-col l-col--sb l-col--fw">

                        <!-- ============================================================================================================================================================================ -->
                        <!-- 記事一覧 6件表示 -->
                        <?php
                            $paged = (int) get_query_var('paged');
                            $args = array(
                                'posts_per_page' => 6,
                                'paged' => $paged,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'post_type' => 'post',
                                'post_status' => 'publish'
                            );
                            $the_query = new WP_Query($args);
                            if ( $the_query->have_posts() ) :
                                while ( $the_query->have_posts() ) : $the_query->the_post();
                        ?>
                            <div class="t-article l-col__3">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="t-article__box">
                                        <p class="t-article__box__category"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></p>
                                        <div class="t-article__box__image"><?php the_post_thumbnail('large_thumbnail', array('alt' => the_title_attribute('echo=0'), 'title' => the_title_attribute('echo=0'))); ?></div>
                                    </div>
                                    <p class="t-article__title"><?php the_title(); ?></p>
                                    <p class="t-article__summary">
                                        <?php 
                                            if(mb_strlen($post->post_content, 'UTF-8') > 50){
                                                $content= mb_substr($post->post_content, 0, 74, 'UTF-8');
                                                echo $content.'';
                                            }else{
                                                echo $post->post_content;
                                            }
                                        ?>
                                    </p>
                                    <p class="t-article__date"><?php echo get_the_date('Y.m.d'); ?></p>
                                </a>
                            </div>
                        <?php endwhile; endif; ?>
                        <!-- ============================================================================================================================================================================ -->



                    </div>
                    <!-- ============================================================================================================================================================================ -->
                    <!-- ページネーション -->
                    <div class="t-pagenation--cover">
                        <div class="t-pagenation">
                            <?php
                                if ($the_query->max_num_pages > 1) {
                                    echo paginate_links(array(
                                        'base' => get_pagenum_link(1) . '%_%',
                                        'format' => 'page/%#%/',
                                        'current' => max(1, $paged),
                                        'total' => $the_query->max_num_pages,
                                        'prev_text' => 'PREV',
                                        'next_text' => 'NEXT'
                                    ));
                                }
                            ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                    <!-- ============================================================================================================================================================================ -->





                </div>
            </div>


<?php get_footer(); ?>