<?php
    if ( in_category( 'business' ) || post_is_in_descendant_category( get_term_by( 'slug', 'business', 'category' ))) {
        include(STYLESHEETPATH.'/single-business.php');
    } else if( in_category( 'recruit' ) || post_is_in_descendant_category( get_term_by( 'slug', 'recruit', 'category' ))) {
        include(STYLESHEETPATH.'/single-recruit.php');
    } else {
        include(STYLESHEETPATH.'/single.php');
    }
    // <!-- if ( in_category(array('business', 'technology', 'marketing', 'system', 'infomation')) ) {
    //     include(STYLESHEETPATH.'/single-business.php');
    // } else if( in_category(array( 'recruiting', 'report' )) ) {
    //     include(STYLESHEETPATH.'/single-recruiting.php');
    // } else {
    //     include(STYLESHEETPATH.'/single.php');
    // } -->
?>

