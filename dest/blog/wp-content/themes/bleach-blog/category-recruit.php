<?php get_header(); ?>


            <div class="l-frame" id="recruiting">
                <h2 class="c-h1"><a href="<?php echo get_rel_home(); ?>"><b>BLEACH</b><br><span>R</span>ecruiting Blog</a></h2>



                <div class="t-tags">
                    <p class="t-tags__select">カテゴリーを選択</p>
                    <?php 
                        $cat = get_category_by_slug("recruit");
                        $cat_id = $cat->cat_ID;

                        $args = array(
                            'orderby' => 'id',
                            'parent' => $cat_id
                        );
                        $categories = get_categories( $args );
                        usort($categories, function($a, $b) {
                            return get_field("表示順", "category_".$a->term_id) - get_field("表示順", "category_".$b->term_id);
                        });
                    ?>
                    <ul class="t-tags__ul l-col l-col--fw">
                        <?php 
                            // echo '<li class="t-tags__ul__item"><a href="/bleach/blog/' . $cat->slug . '/">' . $cat->name . '</a></li>'
                        ?>
                        <?php
                            foreach ( $categories as $category ) {
                                $cat_link = get_category_link($category->cat_ID);
                                echo '<li class="t-tags__ul__item"><a href="' . $cat_link . '">' . $category->name . '</a></li>';
                            }
                        ?>
                    </ul>
                    <div class="t-tags__btn"><a href="#"><img src="<?php bloginfo('template_url'); ?>/asset/image/recruiting-open.png" alt=""></a></div>
                </div>



                <div class="l-main">
                    <div class="t-article--cover l-col l-col--sb l-col--fw">

                        <!-- ============================================================================================================================================================================ -->
                        <?php
                            if (have_posts()) : 
                                while (have_posts()) :
                                    the_post();
                                    get_template_part('inc/each_exrpt_post');
                                endwhile;
                            endif;
                        ?>
                        <!-- ============================================================================================================================================================================ -->

                    </div>
                    <!-- ============================================================================================================================================================================ -->
                    <!-- ページネーション -->
                    <?php get_template_part('inc/pagination'); ?>
                    <!-- ============================================================================================================================================================================ --><!-- ============================================================================================================================================================================ -->
                    <!-- メンバーインタビュー -->
                    <?php get_template_part('inc/member-interview'); ?>
                    <!-- ============================================================================================================================================================================ -->

                </div>
            </div>

            <!-- ============================================================================================================================================================================ -->
            <!-- TOPICS 話題の記事 -->
            <div class="t-topics" id="topics">
                <div class="l-frame">
                    <div class="c-h3">Topics</div>
                    
                    <div class="t-article--cover l-col l-col--sb l-col--fw">
                        <?php
                            $top_page = get_page_by_title('トップページ');
                            $top_page_id = $top_page->ID;
                            $post_object = get_field('post_projects', $top_page_id);
                            $args = array(
                                'post_type' => 'post',
                                'post__in' => $post_object,
                                'posts_per_page' => -1
                            );

                            $the_query = new WP_Query( $args );

                            // The Loop
                            if ( $the_query->have_posts() ) {
                                
                                while ( $the_query->have_posts() ) {
                                    $the_query->the_post();
                                    get_template_part('inc/topics_exrpt_post');
                                }
                                
                                /* Restore original Post Data */
                                wp_reset_postdata();
                            }
                        ?>
                    </div>
                </div>
            </div>

<?php get_footer(); ?>