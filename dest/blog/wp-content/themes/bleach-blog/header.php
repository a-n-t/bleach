<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width">
		<meta name="format-detection" content="telephone=no">
		<meta name="robots" content="index,follow">
		<!-- main title-->
		<title><?php bloginfo('name'); ?></title>
		<!-- meta tags-->
		<meta name="description" content="統合型デジタルマーケティングでD2C支援No.1を目指す株式会社ブリーチです。私たちは、「ココロを共感させるマーケティング力」と「圧倒的な数字の分析力」、「成果を出す強い意志」を武器に社会のニーズに対して新しい解決策を生み出していきます。 ">
		<meta property="og:title" content="株式会社ブリーチ採用情報">
		<meta property="og:type" content="website">
		<meta property="og:image" content="http://bleach.co.jp/og.png">
		<meta property="og:url" content="http://bleach.co.jp/recruit.html">
		<meta property="og:description" content="統合型デジタルマーケティングでD2C支援No.1を目指す株式会社ブリーチです。私たちは、「ココロを共感させるマーケティング力」と「圧倒的な数字の分析力」、「成果を出す強い意志」を武器に社会のニーズに対して新しい解決策を生み出していきます。 ">
		<!-- favicon-->
		<link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">
		<!-- google fonts-->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
		<!-- stylesheets-->
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <?php wp_head(); ?>
	</head>
	<body class="blog">
		<div class="l-limit">
			<div class="l-header">
				<div class="l-line"><span></span></div>
				<h1 class="l-header__logo"><a href="<?php echo get_rel_home(); ?>../" title="株式会社ブリーチ"><img src="<?php bloginfo('template_url'); ?>/asset/image/logo.png" alt="株式会社ブリーチ"></a></h1>
				<div class="l-col l-col--sb l-header__gnav">
					<div class="l-nav l-col l-col--sb">
						<ul class="l-nav__ul l-nav__ul--main l-col l-col--sb">
							<li class="l-nav__ul__item"><a class="bar-home" href="<?php echo get_rel_home(); ?>../">Home</a></li>
							<li class="l-nav__ul__item"><a class="bar-company" href="<?php echo get_rel_home(); ?>../company/">Company</a></li>
							<li class="l-nav__ul__item"><a class="bar-vision" href="<?php echo get_rel_home(); ?>../vision/">Vision</a></li>
							<li class="l-nav__ul__item"><a class="bar-service" href="<?php echo get_rel_home(); ?>../service/">Service</a></li>
							<li class="l-nav__ul__item l-nav__ul__item--btn">Interview<span></span>
								<ul class="l-nestnav">
                                    <li class="l-nestnav__item"><a href="<?php echo get_rel_home(); ?>../interview/ohira.html">代表取締役社長（大平啓介）</a></li>
									<li class="l-nestnav__item"><a href="<?php echo get_rel_home(); ?>../interview/mizutani.html">マネージャー（水谷慶）</a></li>
									<li class="l-nestnav__item"><a href="<?php echo get_rel_home(); ?>../interview/watanabe.html">マネージャー（渡邉緑）</a></li>
									<li class="l-nestnav__item"><a href="<?php echo get_rel_home(); ?>../interview/nakamura.html">セールスライター（中村悠人）</a></li>
									<li class="l-nestnav__item"><a href="<?php echo get_rel_home(); ?>../interview/shimizu.html">セールスライター（清水里沙）</a></li>
									<li class="l-nestnav__item"><a href="<?php echo get_rel_home(); ?>../interview/nishimura.html">グロースハッカー（西村政晃）</a></li>
									<li class="l-nestnav__item"><a href="<?php echo get_rel_home(); ?>../interview/suzuki.html">セールスライター（鈴木大志）</a></li>
								</ul>
							</li>
							<li class="l-nav__ul__item"><a class="bar-recruiting" href="<?php echo get_rel_home(); ?>../recruiting/">Recruiting</a></li>
						</ul>
					</div>
					<div class="l-nav l-col l-col--sb">
						<ul class="l-nav__ul l-nav__ul--sub l-col l-col--sb l-col--sp">
							<li class="l-nav__ul__item l-nav__ul__item--entry l-nav__ul__item--bg"><a class="c-bg" href="<?php echo get_rel_home(); ?>">Blog</a></li>
							<li class="l-nav__ul__item l-nav__ul__item--entry l-nav__ul__item--ng"><a class="c-ng" href="https://job.mynavi.jp/19/pc/corpinfo/displaySeminar/index?optNo=SHhL-C&amp;corpId=229819" target="_blank">新卒エントリー</a></li>
							<li class="l-nav__ul__item l-nav__ul__item--entry l-nav__ul__item--mc"><a class="c-mc" href="https://www.wantedly.com/companies/bleach/" target="_blank">中途エントリー</a></li>
						</ul>
					</div>
				</div>
            </div>