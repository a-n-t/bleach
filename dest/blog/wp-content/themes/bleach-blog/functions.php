<?php
    /**
     * アイキャッチ画像に対応する
     */
    function my_after_setup_theme(){
        // アイキャッチ画像を有効にする
        add_theme_support( 'post-thumbnails' ); 

        // アイキャッチ画像サイズを指定する（横：640px 縦：384）
        // 画像サイズをオーバーした場合は切り抜き
        set_post_thumbnail_size( 640, 384, true ); 
    }
    add_action( 'after_setup_theme', 'my_after_setup_theme' );

    /**
     * 人気記事出力用
     */
    function savePostViews($postID) {
        $_date   = date('Ymd');
        $metaKey = 'viewCount';
        $count   = get_post_meta($postID, $metaKey, true);

        if (empty($count)) {
            $count = 1;
            delete_post_meta($postID, $metaKey);
            add_post_meta($postID, $metaKey, '1');

            $_SESSION["{$_date}_{$postID}"] = true;	//保存日をセッションに格納する
        } else {
            if (!isset($_SESSION["{$_date}_{$postID}"])) {
                $count++;
                update_post_meta($postID, $metaKey, $count);

                $_SESSION["{$_date}_{$postID}"] = true;	//保存日をセッションに格納する
            }
        }
    }

    /**
     * 子カテゴリ分岐
     */
    function post_is_in_descendant_category( $cats, $_post = null ) {
    foreach ( (array) $cats as $cat ) {
        // get_term_children() accepts integer ID only
        $descendants = get_term_children( (int) $cat, 'category');
        if ( $descendants && in_category( $descendants, $_post ) )
        return true;
    }
        return false;
    }

    /**
     * 絶対パス→相対パス
     */
    function make_href_root_relative($input) {
        return preg_replace('!http(s)?://' . $_SERVER['SERVER_NAME'] . '/!', '/', $input);
    }

    function get_rel_home() {
        // WPホームURLと現在のURLから階層数を算出
        $level = substr_count (
            str_replace(
                preg_replace('/\/$/','',home_url()) . '/' ,
                '' ,
                ($_SERVER["HTTPS"]==''|$_SERVER["HTTPS"]==='off' ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]
                //                                           ↑ IIS対応
            ) ,
            '/'
        );
        // 相対パスを生成して返却。
        $path = './';
        if($level>0){
            $path_array = array();
            for($i=1; $i<=$level; $i++){
                $path_array[] = '../';
            }
            $path = join('', $path_array);
        }
        return $path;
    }


    // カスタム投稿タイプを定義
    add_action( 'init', 'register_cpt_team' );
    
    function register_cpt_team() {
    
        $labels = array(
            'name' => _x( 'メンバー', 'team' ),
            'singular_name' => _x( 'メンバー', 'team' ),
            'add_new' => _x( '新規追加', 'team' ),
            'add_new_item' => _x( '新しいメンバープロフィールを追加', 'team' ),
            'edit_item' => _x( 'メンバープロフィールを編集', 'team' ),
            'new_item' => _x( '新しいメンバー', 'team' ),
            'view_item' => _x( 'メンバープロフィールを見る', 'team' ),
            'search_items' => _x( 'メンバー検索', 'team' ),
            'not_found' => _x( 'プロフィールが見つかりません', 'team' ),
            'not_found_in_trash' => _x( 'ゴミ箱にプロフィールはありません', 'team' ),
            'parent_item_colon' => _x( '親メンバー:', 'team' ),
            'menu_name' => _x( 'メンバー', 'team' ),
        );
    
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'description' => '経歴紹介とか',
            'supports' => array( 'title', 'thumbnail', 'page-attributes'),
            
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            
            'show_in_nav_menus' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'has_archive' => true,
            'query_var' => true,
            'can_export' => true,
            'rewrite' => array('with_front' => false),
            'capability_type' => 'post'
        );
    
        register_post_type( 'team', $args );
    }
    
    // サムネイル画像を利用
    add_theme_support( 'post-thumbnails', array( 'team' ) );
    set_post_thumbnail_size();
    
    // アイコンを追加
    function add_menu_icons_styles(){
        echo '<style>
            #adminmenu #menu-posts-team div.wp-menu-image:before {
                content: "\f307";
            }
        </style>';
    }
    add_action( 'admin_head', 'add_menu_icons_styles' );
