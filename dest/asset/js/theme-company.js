/**
 * Company - 
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
site.setSlick = function($) {
    var $heroContent = $('.c-main__slide');

    $heroContent.slick({
        autoplay: true,
        fade: true,
        autoplaySpeed: 4000,
        speed: 1000
    });
}


/*
 * 読み込み完了時の処理
 */
jQuery(function($) {
    site.setSlick($);
});